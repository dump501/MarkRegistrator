//set the base url global variable
var baseUrl = document.querySelector('input[name="baseUrl"]').value;
//call to function to automatically load data
fetchMarks();

/**
 * function that fetch marks
 */
function fetchMarks(){
    var classroomId = document.querySelector('input[name="classroom_id"]').value;
    var subjectId = document.querySelector('input[name="subject_id"]').value;
    var sequence = document.querySelector('input[name="sequence"]').value;
    //console.log(classroomId);
    //fetch all marks
    var postData = new FormData();
    postData.append('test', 123);
    postData.append('classroom_id', classroomId);
    postData.append('subject_id', subjectId);
    postData.append('sequence', sequence);
    
    fetch(baseUrl + 'admin_dashboard/mark/api/get_marks',{
        method: "POST",
        body: postData,
        //headers: {"Content-type": "application/json; charset=UTF-8"}
      })
      .then((serverResponse)=> serverResponse.json())
      .then((response) => {
        console.log(response);
        loadMarks(response);
      },
      (error)=>{
        console.log(error);
      })
};

/**
 * function that load marks
 */

 function loadMarks(marks){
   //alert("ff");
   console.log(marks.response[0]);
    const studentMarks = document.querySelectorAll('.student_mark');
    i=0;
    for(j=0; j<studentMarks.length; j++)
    {
      if( i== marks.response.length)
      {
        //alert('break');
        break;
      }
      else
      {
        if(studentMarks[i].getAttribute('name') == marks.response[i].student_matricule){
          //alert(1);
          input = studentMarks[i];
          //console.log(input.value);
          input.value = marks.response[i].mark;
        }
        i++;
      }
    }
 }

 /**
  * function thats update the marks
  */
function updateMarks()
{
  const studentMarks = document.querySelectorAll('.student_mark');
  var subjectId = document.querySelector('input[name="subject_id"]').value;
  var sequence = document.querySelector('input[name="sequence"]').value;
  var studentFinalMarks = [];
  for(i=0; i<studentMarks.length; i++)
  {
    var obj = {
      name: studentMarks[i].name,
      mark: studentMarks[i].value
    }
    console.log(obj);
    studentFinalMarks.push(obj);
  }
  //console.log(JSON.stringify(studentMarks));
  var postData = new FormData();
  postData.append("studentMarks", JSON.stringify(studentFinalMarks));
  postData.append("subject_id", subjectId);
  postData.append("sequence", sequence);
  fetch(baseUrl + 'admin_dashboard/mark/api/update_marks',{
        method: "POST",
        body: postData,
        //headers: {"Content-type": "application/json; charset=UTF-8"}
      })
      .then((serverResponse)=> serverResponse.json())
      .then((response) => {
        console.log(response);
        //fetchMarks();
      },
      (error)=>{
        console.log(error);
      })
}