<?php
/**
 * by dump501
 * +237698406818/+237650752454
 * tsafack07albin@gmail.com
 * http://gitlab.com/dump501
 */
class LoginModel extends CI_Model
{
    function canLogin($login, $password)
    {
        $this->db->where('login', $login);
        $query = $this->db->get('teachers');
        if($query->num_rows() > 0)
        {
            foreach($query->result() as $row)
            {
                $storePassword = $this->encrypt->decode($row->password);
                if($password == $storePassword)
                {
                    if($row->is_admin == 1)
                    {
                        $auth = array(['id'=> $row->id,
                                       'isAdmin' => $row->is_admin,
                                       'name' => $row->name]);
                        $this->session->set_userdata('auth', $auth);
                        return 'admin';
                    }
                    else
                    {
                        $auth = array(['id'=> $row->id, 
                                       'isAdmin' => $row->is_admin,
                                       'name' => $row->name]);
                        $this->session->set_userdata('auth', $auth);
                    }
                }
                else
                {
                    return 'wrong password';
                }
            }
        }
        else
        {
            return 'Wrong login';
        }
    }
}