<?php
/**
 * by dump501
 * +237698406818/+237650752454
 * tsafack07albin@gmail.com
 * http://gitlab.com/dump501
 */

class Enrol extends CI_Model
{

    public $id;
    public $subject_id;
    public $classroom_id;
    public $acad_year = '2020/2021';

    public function save()
    {
        $data = array(
            'subject_id' => $this->subject_id,
            'classroom_id' => $this->classroom_id,
            'acad_year' => $this->acad_year
        );

        $this->db->insert('enrols', $data);
    }

    public function all()
    {
        return  $this->db->get('enrols');
    }

    public function delete()
    {
        $this->db->delete('enrols', array('id' => $this->id));
    }
}