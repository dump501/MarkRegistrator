<?php
/**
 * by dump501
 * +237698406818/+237650752454
 * tsafack07albin@gmail.com
 * http://gitlab.com/dump501
 */

class Subject extends CI_Model
{

    public $id;
    public $code;
    public $name;
    public $teacher_id;
    public $classroom_id;
    public $acad_year = '2020/2021';

    public function save()
    {
        $data = array(
            'code' => $this->code,
            'name' => $this->name,
            'teacher_id' => $this->teacher_id,
            'classroom_id' => $this->classroom_id,
            'acad_year' => $this->acad_year
        );

        $this->db->insert('subjects', $data);
    }

    public function all()
    {
        return  $this->db->get('subjects');
    }

    public function delete()
    {
        $this->db->delete('subjects', array('id' => $this->id));
    }
}