<?php
/**
 * by dump501
 * +237698406818/+237650752454
 * tsafack07albin@gmail.com
 * http://gitlab.com/dump501
 */

class Student extends CI_Model
{

    public $id;
    public $matricule;
    public $name;
    public $parent_phone;
    public $classroom_id;
    public $acad_year = '2020/2021';

    public function save()
    {
        $data = array(
            'matricule' => $this->matricule,
            'name' => $this->name,
            'parent_phone' => $this->parent_phone,
            'classroom_id' => $this->classroom_id,
            'acad_year' => $this->acad_year
        );

        $this->db->insert('students', $data);
    }

    public function all()
    {
        return  $this->db->get('students');
    }

    public function delete()
    {
        $this->db->delete('students', array('id' => $this->id));
    }

    public function byClassroom()
    {
        $this->db->where('classroom_id', $this->classroom_id);
        $this->db->from('students');
        return $this->db->get();
    }
}