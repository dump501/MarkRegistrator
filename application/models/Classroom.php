<?php
/**
 * by dump501
 * +237698406818/+237650752454
 * tsafack07albin@gmail.com
 * http://gitlab.com/dump501
 */

class Classroom extends CI_Model
{

    public $id;
    public $name;
    public $acad_year = '2020/2021';

    public function __construct()
    {
        $this->load->model('Subject');
    }

    public function save()
    {
        $data = array(
            'name' => $this->name,
            'acad_year' => $this->acad_year
        );

        $this->db->insert('classrooms', $data);
    }

    public function all()
    {
        return  $this->db->get('classrooms');
    }

    public function delete()
    {
        $this->db->delete('classrooms', array('id' => $this->id));
    }

    public function getOwn()
    {
        /*$data = array();
        $i =0;
        $this->db->select('id');
        $this->db->where('teacher_id', $this->session->userdata('auth')[0]['id']);
        $query = $this->db->get('subjects')->result();
        foreach($query as $row)
        {
            $classroomNameQuery = $this->db->query('SELECT * FROM classrooms JOIN enrols on classrooms.id = enrols.classroom_id WHERE enrols.subject_id = ?', array($row->id))->result();
            $data[$i] = $classroomNameQuery;
            $i = $i + 1;
        }*/
        return $this->db->query('SELECT classrooms.name as classroom_name, classrooms.id as id, 
        subjects.name as subject_name, subjects.id as subject_id FROM classrooms, subjects WHERE 
        classrooms.id in (SELECT classroom_id from subjects WHERE subjects.teacher_id = ' .$this->session->userdata('auth')[0]['id'] .' )and subjects.id
         in (SELECT id from subjects WHERE subjects.teacher_id = ? ) and classrooms.id = 
         subjects.classroom_id', array('id'=>$this->session->userdata('auth')[0]['id']));
    }
    public function getAdminOwn()
    {
        return $this->db->query('SELECT classrooms.name as classroom_name, classrooms.id as id, 
        subjects.name as subject_name, subjects.id as subject_id FROM classrooms, subjects WHERE 
        classrooms.id in (SELECT id from classrooms)and subjects.id
         in (SELECT id from subjects) and classrooms.id = 
         subjects.classroom_id');
    }
}