<?php
/**
 * by dump501
 * +237698406818/+237650752454
 * tsafack07albin@gmail.com
 * http://gitlab.com/dump501
 */

class Teacher extends CI_Model
{

    public $id;
    public $name;
    public $phone;
    public $login;
    public $password;
    public $acad_year = '2020/2021';

    public function save()
    {
        $data = array(
            'name' => $this->name,
            'phone' => $this->phone,
            'login' => $this->login,
            'password' => $this->password,
            'acad_year' => $this->acad_year
        );

        $this->db->insert('teachers', $data);
    }

    public function all()
    {
        return  $this->db->get('teachers');
    }

    public function delete()
    {
        $this->db->delete('teachers', array('id' => $this->id));
    }
}