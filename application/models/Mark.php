<?php
/**
 * by dump501
 * +237698406818/+237650752454
 * tsafack07albin@gmail.com
 * http://gitlab.com/dump501
 */

class Mark extends CI_Model
{

    public $id;
    public $student_matricule;
    public $subject_id;
    public $mark;
    public $sequence = 1;
    public $acad_year = '2020/2021';

    public function __construct()
    {
        $this->load->model('Student');
    }

    public function save()
    {
        $data = array(
            'student_matricule' => $this->student_matricule,
            'subject_id' => $this->subject_id,
            'mark' => $this->mark,
            'sequence' => $this->sequence,
            'acad_year' => $this->acad_year
        );

        $this->db->insert('marks', $data);
    }

    public function update()
    {
        $data = array(
            'student_matricule' => $this->student_matricule,
            'subject_id' => $this->subject_id,
            'mark' => $this->mark,
            'sequence' => $this->sequence,
            'acad_year' => $this->acad_year
        );
        $this->db->set('mark', $this->mark);
        $this->db->where('student_matricule', $this->student_matricule);
        $this->db->where('subject_id', $this->subject_id);
        $this->db->where('sequence', $this->sequence);
        $this->db->where('acad_year', $this->acad_year);
        $this->db->update('marks');
    }

    public function  byClassroomAndSubject()
    {
        $this->db->where('subject_id', $this->subject_id);
        $this->db->where('sequence', $this->sequence);
        $this->db->from('marks');
        return $this->db->get()->result();
    }

    public function delete()
    {
        $this->db->delete('teachers', array('id' => $this->id));
    }

    public function byStudentAndSequence()
    {
        $query = $this->db->query("SELECT *, subjects.name as subject_name FROM marks INNER JOIN 
        subjects on marks.subject_id = subjects.id 
        INNER JOIN students on marks.student_matricule = students.matricule
        WHERE marks.sequence = $this->sequence AND marks.student_matricule = '$this->student_matricule'");
        return $query->result();
    }
}