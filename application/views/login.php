<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?= base_url('css/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('css/style.css') ?>">
    <title>Login</title>
</head>
<body class="bg-indigo">
    <div class="container p-5">
        <div class="row p-5">
            <div class="col-md-3"></div>
            <div class="col-12 col-md-6 p-5">
                <div class="card shadow bg-white p-5 rounded">
                    <center>
                    <H1 class="text-dark">Login page</H1>
                    </center>
                    <form class="d-flex flex-column" action="<?= base_url('post_login') ?>" method="post">
                        <div class="mb-4">
                            <input type="text" class="form-control" name="login" value="<?= set_value('login') ?>" placeholder="enter your login">
                            <span class="text-danger"><?= form_error('login') ?></span>
                        </div>
                        <div class="mb-4">
                            <input type="password" class="form-control" name="password" value="<?= set_value('password') ?>" placeholder="enter your password">
                            <span class="text-danger"><?= form_error('password') ?></span>
                        </div>
                        <button class="btn bg-indigo d-block" type="submit">Login</button>
                    </form>
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>
    </div>
</body>
<script src="js/bootstrap.min.js"></script>
</html>