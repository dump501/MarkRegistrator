<?php $this->load->view('header'); $this->load->view('admin/navbar');?>

<div>
    <a href="<?= base_url('admin_dashboard/enrol/create') ?>">New enrol</a>
    <table style="border: 1px solide">
        <thead>
            <td>Classroom</td>
            <td>Subject</td>
            <td>Action</td>
        </thead>
        <tbody>
            <?php
            foreach($enrols as $enrol)
            {
            ?>
            <tr>
                <td><?= $enrol->classroom_name ?></td>
                <td><?= $enrol->subject_name ?></td>
                <td><a href="<?= site_url("admin_dashboard/enrol/delete/$enrol->id") ?>">delete</a></td>
            </tr>
            <?php
            }
            ?>
        </tbody>
    </table>
</div>

<?php $this->load->view('footer');?>