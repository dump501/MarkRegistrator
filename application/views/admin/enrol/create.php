<?php $this->load->view('header'); $this->load->view('admin/navbar');?>

<div>
    <a href="<?= base_url('admin_dashboard/enrol') ?>">list enrol</a><br>
    <form action="<?= site_url('admin_dashboard/enrol/store')?>" method="post">
        <select name="subject_id" id="">
            <option selected>Select the subject</option>
            <?php
            foreach($subjects as $subject)
            {
            ?>
            <option value="<?= $subject->id ?>"><?= $subject->name ?></option>
            <?php
            }
            ?>
        </select>
        <span><?= form_error('subject_id') ?></span>
        <select name="classroom_id" id="">
            <option selected>Select the Classroom</option>
            <?php
            foreach($classrooms as $classroom)
            {
            ?>
            <option value="<?= $classroom->id ?>"><?= $classroom->name ?></option>
            <?php
            }
            ?>
        </select>
        <span><?= form_error('classroom_id') ?></span>
        <button type="submit">Create</button>
    </form>
</div>

<?php $this->load->view('footer');?>