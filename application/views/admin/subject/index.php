<?php $this->load->view('header'); $this->load->view('admin/navbar');?>

<div class="col-12">
	<div class="card shadow-lg p-3 bg-white rounded">
		<div class="card-body">
			<div class="card-title">
				<h1 class="mb-4">List of Subjects</h1>
            </div>
            <div class="d-flex flex-row justify-content-end mb-2">
			    <a class="btn bg-indigo" href="<?= base_url('admin_dashboard/subject/create') ?>">New Subject</a>
            </div>
			<table class="table table-bordered">
				<thead>
					<tr>
                        <th>Code</th>
                        <th>Name</th>
                        <th>Teacher</th>
                        <th>Classroom</th>
                        <th>Actions</th>
                    </tr>
				</thead>
				<tbody>
					<?php
            foreach($subjects as $subject)
            {
            ?>
					<tr>
						<td><?= $subject->code ?></td>
						<td><?= $subject->subject_name ?></td>
						<td><?= $subject->teacher_name  ?></td>
						<td><?= $subject->classroom_name  ?></td>
						<td>
							<a class="btn btn-warning" href="<?= site_url("admin_dashboard/subject/") ?>">update</a>
							<a class="btn btn-danger" href="<?= site_url("admin_dashboard/subject/delete/$subject->id") ?>">delete</a>
						</td>
					</tr>
					<?php
            }
            ?>
				</tbody>
			</table>
		</div>
	</div>
</div>

<?php $this->load->view('footer');?>