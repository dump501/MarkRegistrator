<?php $this->load->view('header'); $this->load->view('admin/navbar');?>

<div class="col-12">
	<div class="card shadow-lg p-3 bg-white rounded">
		<div class="card-body">
			<div class="card-title">
                <h1 class="mb-4">Add Subject</h1>
			</div>
            <form action="<?= site_url('admin_dashboard/subject/store')?>" method="post">
                <div class="mb-3">
                    <input type="text" class="form-control" name="code" value="<?= set_value('code') ?>" placeholder="Subject code">
                    <span class="text-danger"><?= form_error('code') ?></span>
                </div>
                <div class="mb-3">
                    <input type="text" class="form-control" name="name" value="<?= set_value('name') ?>" placeholder="Subject Name">
                    <span class="text-danger"><?= form_error('name') ?></span>
                </div>
                <div class="mb-3">
                    <select class="form-control" name="teacher_id" id="">
                        <option selected>Select the teacher</option>
                        <?php
                        foreach($teachers as $teacher)
                        {
                        ?>
                            <option value="<?= $teacher->id ?>"><?= $teacher->name ?></option>
                            <?php
                        }
                        ?>
                    </select>
                    <span class="text-danger"><?= form_error('teacher_id') ?></span>
                </div>
                <div class="mb-3">
                    <select class="form-control" name="classroom_id" id="">
                        <option selected>Select the Classroom</option>
                        <?php
                        foreach($classrooms as$classroom)
                        {
                        ?>
                        <option value="<?= $classroom->id ?>"><?= $classroom->name ?></option>
                        <?php
                        }
                        ?>
                    </select>
                    <span class="text-danger"><?= form_error('classroom_id') ?></span>
                </div>
				<div>
                <button class="btn bg-indigo" type="submit">Create</button>
                <button class="btn btn-danger" type="reset">Cancel</button>
                </div>
			</form>
		</div>
	</div>
</div>

<?php $this->load->view('footer');?>