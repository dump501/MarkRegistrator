<?php $this->load->view('header'); $this->load->view('admin/navbar');?>

<div>
    <a href="<?= base_url('admin_dashboard/classroom') ?>">list Classroom</a><br>
    <form action="<?= site_url('admin_dashboard/classroom/store')?>" method="post">
        <input type="text" name="name" value="<?= set_value('name') ?>" placeholder="Classroom Name">
        <span><?= form_error('name') ?></span>
        <button type="submit">Create</button>
    </form>
</div>

<?php $this->load->view('footer');?>