<?php $this->load->view('header'); $this->load->view('admin/navbar');?>

<div class="col-12 px-5">
    <div class="card shadow bg-white p-3 rounded">
        <div class="card-body">
            <h1 class="card-title mb-4">List of classroom</h1>
            <div class="">
                <form method="POST" class="d-flex" action="<?= site_url('admin_dashboard/classroom/store')?>">
                    <input type="text" class="form-control mx-2" name="name" value="<?= set_value('name') ?>" placeholder="Classroom Name">
                    <span class="text-danger"><?= form_error('name') ?></span>
                    <button class="btn bg-indigo" type="submit">Create</button>
                </form>
            </div>
            <div class="row">
                <?php
                    foreach($classrooms as $classroom)
                    {
                ?>
                    <div class="col-12 col-md-3 gy-5">
                        <div class="card gx-5 shadow-sm p-4 bg-indigo-light rounded">
                            <div class="card-body">
                                <div class="card-title">
                                    <H3 class="text-white text-uppercase mb-3"><?= $classroom->name ?></H3>
                                </div>
                                <a href='<?= base_url("admin_dashboard/classroom/delete/$classroom->id") ?>' class="btn btn-danger d-block">Delete</a>
                            </div>
                        </div>
                    </div>
                <?php
                    }
                ?>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('footer');?>