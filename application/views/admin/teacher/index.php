<?php $this->load->view('header'); $this->load->view('admin/navbar');?>

<div class="col-12">
    <div class="card shadow-lg p-3 bg-white rounded">
    	<div class="card-body">
            <div class="card-title">
                <h1 class="">List of registrated teachers</h1>
            </div>
    		<div class="d-flex flex-row justify-content-end mb-3">
    			<a href="<?= base_url('admin_dashboard/teacher/create') ?>" class="btn btn-success bg-indigo">Add
    				teacher</a>
    		</div>
    		<table class="table table-bordered">
    			<thead>
    				<tr>
    					<th>Name</th>
    					<th>Phone</th>
    					<th>Academic year</th>
    					<th>Actions</th>
    				</tr>
    			</thead>
    			<tbody>
    				<?php
            foreach($teachers as $teacher)
            {
            ?>
    				<tr>
    					<td><?= $teacher->name ?></td>
    					<td><?= $teacher->phone ?></td>
    					<td><?= $teacher->acad_year ?></td>
    					<td><a href="<?= site_url("admin_dashboard/teacher/delete/$teacher->id") ?>"
    							class="btn btn-danger">delete</a></td>
    				</tr>
    				<?php
            }
            ?>
    			</tbody>
    		</table>
    	</div>
    </div>
</div>

<?php $this->load->view('footer');?>