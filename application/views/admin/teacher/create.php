<?php $this->load->view('header'); $this->load->view('admin/navbar');?>

<div class="col-12">
    <div class="card shadow-lg p-3 bg-white rounded">
        <div class="card-body">
            <div class="card-title">
                <h1 class="mb-4">Register a Teacher</h1>
            </div>
            <form action="<?= site_url('admin_dashboard/teacher/store')?>" method="post">
                <div class="mb-3">
                    <label for="name" class="fw-bold mb-2">Teacher's name</label>
                    <input type="text" name="name" class="form-control" value="<?= set_value('name') ?>" placeholder="Name">
                    <span class="text-danger"><?= form_error('name') ?></span>
                </div>
                <div class="mb-3">
                    <label for="phone" class="fw-bold mb-2">Teacher's phone</label>
                    <input type="tel" name="phone" class="form-control" value="<?= set_value('phone') ?>" placeholder="phone number">
                    <span class="text-danger"><?= form_error('phone') ?></span>
                </div>
                <div class="mb-3">
                    <label for="login" class="fw-bold mb-2">Teacher's password</label>
                    <input type="text" name="login" class="form-control" value="<?= set_value('login') ?>" placeholder="Login">
                    <span class="text-danger"><?= form_error('login') ?></span>
                </div>
                <div class="mb-3">
                    <label for="password" class="fw-bold mb-2">Teacher's password</label>
                    <input type="text" name="password" class="form-control" placeholder="Password">
                    <span class="text-danger"><?= form_error('password') ?></span>
                </div>
                <div class="mt-4">
                    <button type="submit" class="btn btn-success bg-indigo">Create</button>
                    <button type="reset" class="btn btn-danger">cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>

<?php $this->load->view('footer');?>