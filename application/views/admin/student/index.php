<?php $this->load->view('header'); $this->load->view('admin/navbar');?>

<div class="col-12">
	<div class="card shadow-lg p-3 bg-white rounded">
		<div class="card-body">
            <div class="card-title">
                <h1>Resgistrated students</h1>
            </div>
			<div class="d-flex flex-row justify-content-end mb-3">
                <a class="btn bg-indigo" href="<?= base_url('admin_dashboard/student/create') ?>">New Student</a>
            </div>
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>Matricule</th>
						<th>Name</th>
						<th>Parent phone</th>
						<th>Academic year</th>
						<th>Actions</th>
					</tr>
				</thead>
				<?php
            foreach($students as $student)
            {
            ?>
				<tr>
					<td><?= $student->matricule ?></td>
					<td><?= $student->name ?></td>
					<td><?= $student->parent_phone ?></td>
					<td><?= $student->acad_year ?></td>
					<td><a class="btn btn-danger"
							href="<?= site_url("admin_dashboard/student/delete/$student->id") ?>">delete</a></td>
				</tr>
				<?php
            }
            ?>
				</tbody>
			</table>
		</div>
	</div>
</div>

<?php $this->load->view('footer');?>