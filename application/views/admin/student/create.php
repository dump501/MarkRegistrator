<?php $this->load->view('header'); $this->load->view('admin/navbar');?>

<div class="col-12">
    <div class="card shadow-lg p-3 bg-white rounded">
        <div class="card-body">
            <div class="card-title">
                <h1 class="mb-4">Register a student</h1>
            </div>
            <form action="<?= site_url('admin_dashboard/student/store')?>" method="post">
                <div class="mb-3">
                    <label class="fw-bold mb-2" for="matricule">Matricule</label>
                    <input type="text" class="form-control" name="matricule" value="<?= set_value('matricule') ?>" placeholder="Matricule">
                    <span><?= form_error('matricule') ?></span>
                </div>
                <div class="mb-3">
                    <label class="fw-bold mb-2" for="matricule">Name</label>
                    <input type="text" class="form-control" name="name" value="<?= set_value('name') ?>" placeholder="Name">
                    <span><?= form_error('name') ?></span>
                </div>
                <div class="mb-3">
                    <label class="fw-bold mb-2" for="matricule">Parent phone number</label>
                    <input type="tel" class="form-control" name="phone" value="<?= set_value('phone') ?>" placeholder="parent phone number">
                    <span><?= form_error('phone') ?></span>
                </div>
        		<div class="mb-3">
                    <label class="fw-bold mb-2" for="matricule">Select the class</label>
                    <select name="classroom_id" class="form-control" id="">
                        <option selected>Select the Classroom</option>
                        <?php
                        foreach($classrooms as $classroom)
                        {
                        ?>
                        <option value="<?= $classroom->id ?>"><?= $classroom->name ?></option>
                        <?php
                        }
                        ?>
                    </select>
        		<span><?= form_error('classroom_id') ?></span>
                </div>
                <div>
                    <button class="btn bg-indigo" type="submit">Create</button>
                    <button class="btn btn-danger" type="reset">Cancel</button> 
                </div>
        	</form>
        </div>
    </div>
</div>

<?php $this->load->view('footer');?>