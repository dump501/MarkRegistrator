<?php $this->load->view('header'); $this->load->view('admin/navbar');?>

<div class="col-12">
    <div class="card shadow-lg p-3 bg-white">
        <div class="card-body">
            <div class="card-title">

            </div>
            <form action="<?= site_url('admin_dashboard/report_booklet/print') ?>" method="post">
                <label for="student_matricule" class="fw-bold mb-2">Select the student you want to print booklet</label>
                <select class="form-control mb-3" name="student_matricule" id="">
                    <option selected>Choose the student</option>
                    <?php
                    foreach($students as $student)
                    {
                    ?>
                        <option value="<?= $student->matricule ?>"><?= $student->name ?></option>
                    <?php
                    }
                    ?>
                </select>
                
                <label for="sequence" class="fw-bold mb-2">Select the sequence</label>
                <select class="form-control mb-3" name="sequence" id="">
                    <option selected>Choose the sequence</option>
                    <option value="1">sequence 1</option>
                    <option value="2">sequence 2</option>
                    <option value="3">sequence 3</option>
                    <option value="4">sequence 4</option>
                    <option value="5">sequence 5</option>
                    <option value="6">sequence 6</option>
                </select>
                <button class="btn bg-indigo" type="submit">print booklet</button>
            </form>
        </div>
    </div>
</div>

<?php $this->load->view('footer');?>