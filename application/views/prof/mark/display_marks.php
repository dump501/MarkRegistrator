<?php $this->load->view('header'); $this->load->view('prof/navbar');?>

<div class="col-12">
    <div class="card sahdow-lg p-3 bg-white rounded" style="margin-bottom: 25vh">
        <div class="card-body">
            <div class="card-title">
                <h1 class="mb-4">Display Marks</h1>
            </div>
            <div class="d-flex flex-row mb-3 justify-content-between">
                <div class="mt-2">
                    <span class="fw-bold">Academic year: </span>
                    <span class="badge bg-indigo"><?= $acad_year ?></span>
                    <span class="fw-bold">sequence: </span>
                    <span class="badge bg-indigo"><?= $sequence ?></span>
                </div>
                <div class="d-flex flex-row">
                    <a href="<?= base_url('dashboard/mark') ?>" class="btn bg-indigo mx-3" onclick="updateMarks()">change classroom / sequence</a>
                    <button class="btn btn-success" onclick="updateMarks()">Validate changes</button>
                </div>
            </div>
                <input type="hidden" name="classroom_id" value="<?= $classroom_id ?>">
                <input type="hidden" name="subject_id" value="<?= $subject_id ?>">
                <input type="hidden" name="sequence" value="<?= $sequence ?>">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>matricule</th>
                                <th>student Name</th>
                                <th>mark</th>
                            </tr>
                        </thead>
                        <tbody>
                                <?php
                                foreach($students as $student)
                                {
                                ?>
                                <tr>
                                    <td><?= $student->matricule ?></td>
                                    <td><?= $student->name ?></td>
                                    <td><input type="number" class="form-control student_mark" name="<?= $student->matricule ?>" value="0" readonly></td>
                                </tr>
                                <?php
                                }
                                ?>
                        </tbody>
                    </table>
        </div>
    </div>
</div>

<?php $this->load->view('footer');?>
<script src="<?= site_url('js/show.js') ?>"></script>