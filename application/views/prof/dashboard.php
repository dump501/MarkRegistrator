<?php $this->load->view('header'); $this->load->view('prof/navbar');?>
<div class="col-12">
    <div class="row">
        <div class="col-12 bg-white">
            <div class="card p-3 shadow-lg rounded">
                <center>
                    <h1><strong>ADMINISTRATOR DASHBOARD</strong></h1>
                </center>
            </div>
        </div>
        <div class="col-6 mt-4">
            <div class="card shadow-lg p-5 rounded bg-indigo">
                <div class="card-body">
                    <div class="card-title">
                        <h1>Teachers: 16</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-6 mt-4">
            <div class="card shadow-lg p-5 rounded bg-indigo">
                <div class="card-body">
                    <div class="card-title">
                        <h1>Subjects: 16</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-6">
            <div class="card shadow-lg p-5 mt-4 rounded bg-indigo">
                <div class="card-body">
                    <div class="card-title">
                        <h1>Students: 16</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-6">
            <div class="card shadow-lg p-5 mt-4  rounded bg-indigo">
                <div class="card-body">
                    <div class="card-title">
                        <h1>Classrooms: 16</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('footer');?>