<div class="container-fluid mb-2">
	<div class="row">
		<nav class="navbar navbar-expand-lg navbar-light bg-indigo shadow-lg">
			<div class="container-fluid">
				<a class="navbar-brand" href="#"><h3 class="text-white">Mark Registrator</h3></a>
				<button class="navbar-toggler" type="button" data-bs-toggle="collapse"
					data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
					aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav me-auto mb-2 mb-lg-0">
						<li class="nav-item">
							<a class="nav-link active fw-bold" aria-current="page"
								href="<?= site_url('dashboard/') ?>">Home</a>
						</li>
						<li class="nav-item">
							<a class="nav-link active fw-bold" aria-current="page"
								href="<?= site_url('dashboard/mark') ?>">Input / edit Marks</a>
						</li>
						<li class="nav-item">
							<a class="nav-link active fw-bold" aria-current="page"
								href="<?= site_url('dashboard/mark/display_form') ?>">Disply Marks</a>
						</li>
					</ul>
					<!--<form class="d-flex">
						<input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
						<button class="btn btn-outline-success" type="submit">Search</button>
					</form>-->
					<ul class="navbar-nav mb-2 mb-lg-0">
						<li class="nav-item mt-2">
							<span class="navbar-text fw-bold text-white">
								<?= ($this->session->userdata('auth')[0]['isAdmin'] == 1) ? 'Admin': 'prof' ?>
							</span>
						</li>
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle fw-bold" href="#" id="navbarDropdown" role="button"
								data-bs-toggle="dropdown" aria-expanded="false">
								<?= $this->session->userdata('auth')[0]['name'] ?>
							</a>
							<ul class="dropdown-menu" aria-labelledby="navbarDropdown">
								<li><a class="dropdown-item" href="<?= site_url('logout') ?>">logout</a></li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</nav>
	</div>
</div>

<?php 
    if($this->session->flashdata('message'))
	{?>
		<div class="alert alert-success alert-dismissible fade show" role="alert">
        	<?= $this->session->flashdata('message') ?>
			<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
	  	</div>
<?php
    }
?>

<div class="container-fluid" id="body">
    <div class="row">
