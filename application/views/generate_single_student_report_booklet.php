<!DOCTYPE html>
<html>
<head>
	<title>Print student report booklet</title>
	<style>
	.table-container{
		padding: 15px;
	}
	table{
		min-width: 100%;
		border-collapse: collapse;
	}
		.align-left{
			text-align: left;
		}
		.align-right{
			text-align: right;
		}
		#title{
			font-weight: bolder;
			font-size: 2rem;
		}
		.mt-3{
			margin-top: 30px;
		}
		table.bordered{
			border: 1px solid;
		}
		table.bordered>tbody> tr {
			border: 1px solid;
		}
		table.bordered>tbody> tr> td {
			border: 1px solid;
		}
		#booklet{
			margin-top: 30px;
		}

		#end{
			margin
		}
	</style>
</head>
<body>

<div class="table-container">
	<table>
		<tr>
			<td class="align-left">
				REPUBLIQUE DU CAMEROUN <br>					
				Paix-Travail-Patrie <br>					
				REGION DE L' OUEST <br>						
				DELEGATION REGIONALES DES ENSEIGNEMENTS SCONDAIRE <br>						
				DELEGATION DEPARTEMENTALE DE LA MIFI <br>	
			</td>
			<td class="align-right">
				REPUBLIQUE DU CAMEROUN <br>						
				Paix-Travail-Patrie <br>						
				REGION DE L' OUEST <br>						
				DELEGATION REGIONALES DES ENSEIGNEMENTS SCONDAIRE <br>						
				DELEGATION DEPARTEMENTALE DE LA MIFI <br>	
			</td>
		</tr>
	</table>
	<table class="mt-3">
		<tr>
			<td id="title">
				<center>REPORT BOOKLET	</center>
			</td>
		</tr>
		<tr>
			<td>
				<center style="text-align: left">
				Noms et Prenoms/Names : <?= $marks[0]->name ?> <br>								
				classroom:  <br>
				Mle/Reg n°: <?= $marks[0]->student_matricule ?>	<br>
				Parent: <?= $marks[0]->parent_phone ?>	<br>
				</center>
			</td>
		</tr>
	</table>
	<table id="booklet" class="bordered">
		<tr>
			<td>
				Subjects
			</td>
			<td>
				sequence n° <?= $marks[0]->sequence ?>
			</td>
			<td>
				coef
			</td>
			<td>
				total
			</td>
		</tr>
		<?php foreach($marks as $mark): ?>
		
		<tr>
			<td>
				<?= $mark->subject_name ?>
			</td>
			<td>
				<?= $mark->mark ?>
			</td>
			<td>
				1
			</td>
			<td>
				<?= $mark->mark ?>
			</td>
		</tr>
		<?php endforeach ?>
	</table>
	<table class="bordered">
		<tr>
			<td>
				Average/ 20: 15.78
			</td>
		</tr>
	</table>
</div>


</body>
</html>