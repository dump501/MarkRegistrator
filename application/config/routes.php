<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'FrontViewController/index';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['generatepdf'] = "welcome/convertpdf";

$route['welcome'] = 'welcome';
$route['/'] = 'FrontViewController/index';
$route['home'] = 'FrontViewController/index';

$route['login'] = 'LoginController/login';
$route['post_login'] = 'LoginController/postLogin';
$route['logout'] = 'LoginController/logout';

############################################################################
//prof area
$route['dashboard'] = 'DashboardController';

//prof marks
$route['dashboard/mark/create'] = 'MarkController/create';
$route['dashboard/mark'] = 'MarkController';
$route['dashboard/mark/show'] = 'MarkController/show';
$route['dashboard/mark/display_form'] = 'MarkController/displayForm';
$route['dashboard/mark/display_marks'] = 'MarkController/displayMarks';

############################################################################
//api routes

//teacher
$route['dashboard/mark/api/get_marks'] = 'MarkController/apiGetMarks';
$route['dashboard/mark/api/update_marks'] = 'MarkController/apiUpdateMarks';

//admin
$route['admin_dashboard/mark/api/get_marks'] = 'AdminMarkController/apiGetMarks';
$route['admin_dashboard/mark/api/update_marks'] = 'AdminMarkController/apiUpdateMarks';

############################################################################
//admin area
$route['admin_dashboard'] = 'AdminDashboardController';

// admin teacher
$route['admin_dashboard/teacher'] = 'AdminTeacherController';
$route['admin_dashboard/teacher/create'] = 'AdminTeacherController/create';
$route['admin_dashboard/teacher/store'] = 'AdminTeacherController/store';
$route['admin_dashboard/teacher/delete/(:any)'] = 'AdminTeacherController/delete/$1';

//admin student
$route['admin_dashboard/student'] = 'AdminStudentController';
$route['admin_dashboard/student/create'] = 'AdminStudentController/create';
$route['admin_dashboard/student/store'] = 'AdminStudentController/store';
$route['admin_dashboard/student/delete/(:any)'] = 'AdminStudentController/delete/$1';

//admin classroom
$route['admin_dashboard/classroom'] = 'AdminClassroomController';
$route['admin_dashboard/classroom/create'] = 'AdminClassroomController/create';
$route['admin_dashboard/classroom/store'] = 'AdminClassroomController/store';
$route['admin_dashboard/classroom/delete/(:any)'] = 'AdminClassroomController/delete/$1';

//admin subject
$route['admin_dashboard/subject'] = 'AdminSubjectController';
$route['admin_dashboard/subject/create'] = 'AdminSubjectController/create';
$route['admin_dashboard/subject/store'] = 'AdminSubjectController/store';
$route['admin_dashboard/subject/delete/(:any)'] = 'AdminSubjectController/delete/$1';

//admin assign subject to classroom
$route['admin_dashboard/enrol'] = 'AdminEnrolController';
$route['admin_dashboard/enrol/create'] = 'AdminEnrolController/create';
$route['admin_dashboard/enrol/store'] = 'AdminEnrolController/store';
$route['admin_dashboard/enrol/delete/(:any)'] = 'AdminEnrolController/delete/$1';

//admin mark 
$route['admin_dashboard/mark'] = 'AdminMarkController';
$route['admin_dashboard/mark/show'] = 'AdminMarkController/show';
$route['admin_dashboard/mark/display_form'] = 'AdminMarkController/displayForm';
$route['admin_dashboard/mark/display_marks'] = 'AdminMarkController/displayMarks';

//admin report booklet

$route['admin_dashboard/report_booklet'] = 'AdminReportBookletController';
$route['admin_dashboard/report_booklet/print'] = 'AdminReportBookletController/print';


