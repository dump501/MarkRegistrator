<?php
/**
 * by dump501
 * +237698406818/+237650752454
 * tsafack07albin@gmail.com
 * http://gitlab.com/dump501
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminClassroomController extends CI_Controller
{
    public $isAdminHookable = TRUE;
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->library('encrypt');
        $this->load->model('Classroom');
    }

    public function index()
    {
        $data['classrooms'] = $this->Classroom->all()->result();
        $this->load->view('admin/classroom/index', $data);
    }

    public function create()
    {
       redirect('admin/classroom');
    }

    public function store()
    {
        $this->form_validation->set_rules('name', 'name', 'required');

        if($this->form_validation->run())
        {
            $classroo = new Classroom();
            $classroo->name = $this->input->post('name');
            $classroo->save();
            redirect('admin_dashboard/classroom');
        }
        else
        {
            $this->load->view('admin/classroom/create');
        }
    }

    public function delete($id)
    {
        $classroom = new Classroom();
        $classroom->id = $id;
        $classroom->delete();
        redirect('admin_dashboard/classroom');
    }
}