<?php
/**
 * by dump501
 * +237698406818/+237650752454
 * tsafack07albin@gmail.com
 * http://gitlab.com/dump501
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminSubjectController extends CI_Controller
{
    public $isAdminHookable = TRUE;
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->library('encrypt');
        $this->load->model(['Subject', 'Teacher', 'Classroom']);
    }

    public function index()
    {
        $query = $this->db->query('SELECT subjects.id as id, teachers.name as teacher_name, subjects.name as subject_name, classrooms.name as classroom_name, code FROM subjects,teachers,classrooms WHERE teachers.id in (SELECT subjects.teacher_id FROM subjects) and  classrooms.id in (SELECT subjects.classroom_id FROM subjects) and subjects.teacher_id = teachers.id and subjects.classroom_id = classrooms.id');
        $data['subjects'] = $query->result();
        $this->load->view('admin/subject/index', $data);
    }

    public function create()
    {
        $data['teachers'] = $this->Teacher->all()->result();
        $data['classrooms'] = $this->Classroom->all()->result();
        $this->load->view('admin/subject/create', $data);
    }

    public function store()
    {
        $this->form_validation->set_rules('code', 'code', 'required');
        $this->form_validation->set_rules('name', 'name', 'required');
        $this->form_validation->set_rules('teacher_id', 'teacher', 'required');
        $this->form_validation->set_rules('classroom_id', 'classroom', 'required');

        if($this->form_validation->run())
        {
            $subject = new Subject();
            $subject->code = $this->input->post('code');
            $subject->name = $this->input->post('name');
            $subject->teacher_id = $this->input->post('teacher_id');
            $subject->classroom_id = $this->input->post('classroom_id');
            $subject->save();
            redirect('admin_dashboard/subject');
        }
        else
        {
            $this->load->view('admin/subject/create');
        }
    }

    public function delete($id)
    {
        $subject = new Subject();
        $subject->id = $id;
        $subject->delete();
        redirect('admin_dashboard/subject');
    }
}