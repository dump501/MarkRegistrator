<?php
/**
 * by dump501
 * +237698406818/+237650752454
 * tsafack07albin@gmail.com
 * http://gitlab.com/dump501
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminTeacherController extends CI_Controller
{
    public $isAdminHookable = TRUE;
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->library('encrypt');
        $this->load->model('Teacher');
    }

    public function index()
    {
        $data['teachers'] = $this->Teacher->all()->result();
        $this->load->view('admin/teacher/index', $data);
    }

    public function create()
    {
        $this->load->view('admin/teacher/create');
    }

    public function store()
    {
        $this->form_validation->set_rules('name', 'name', 'required');
        $this->form_validation->set_rules('phone', 'phone', 'required');
        $this->form_validation->set_rules('login', 'login', 'required');
        $this->form_validation->set_rules('password', 'password', 'required');

        if($this->form_validation->run())
        {
            $teacher = new Teacher();
            $teacher->name = $this->input->post('name');
            $teacher->phone = $this->input->post('phone');
            $teacher->login = $this->input->post('login');
            $teacher->password = $this->encrypt->encode($this->input->post('password'));
            $teacher->save();
            redirect('admin_dashboard/teacher');
        }
        else
        {
            $this->load->view('admin/teacher/create');
        }
    }

    public function delete($id)
    {
        $teacher = new Teacher();
        $teacher->id = $id;
        $teacher->delete();
        redirect('admin_dashboard/teacher');
    }
}