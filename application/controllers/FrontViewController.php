<?php
/**
 * by dump501
 * +237698406818/+237650752454
 * tsafack07albin@gmail.com
 * http://gitlab.com/dump501
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class FrontViewController extends CI_Controller
{
    public function index()
    {
        $this->load->view('home');
    }
}