<?php
/**
 * by dump501
 * +237698406818/+237650752454
 * tsafack07albin@gmail.com
 * http://gitlab.com/dump501
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminStudentController extends CI_Controller
{
    public $isAdminHookable = TRUE;
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->library('encrypt');
        $this->load->model(['Student', 'Classroom']);
    }

    public function index()
    {
        $data['students'] = $this->Student->all()->result();
        $this->load->view('admin/student/index', $data);
    }

    public function create()
    {
        $data['classrooms'] = $this->Classroom->all()->result();
        $this->load->view('admin/student/create', $data);
    }

    public function store()
    {
        $this->form_validation->set_rules('matricule', 'matricule', 'required');
        $this->form_validation->set_rules('name', 'name', 'required');
        $this->form_validation->set_rules('phone', 'phone', 'required');
        $this->form_validation->set_rules('classroom_id', 'classroom', 'required');

        if($this->form_validation->run())
        {
            $student = new Student();
            $student->matricule = $this->input->post('matricule');
            $student->name = $this->input->post('name');
            $student->parent_phone = $this->input->post('phone');
            $student->classroom_id = $this->input->post('classroom_id');
            $student->save();
            redirect('admin_dashboard/student');
        }
        else
        {
            $this->load->view('admin/student/create');
        }
    }

    public function delete($id)
    {
        $student = new Student();
        $student->id = $id;
        $student->delete();
        redirect('admin_dashboard/student');
    }
}