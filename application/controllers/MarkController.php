<?php
/**
 * by dump501
 * +237698406818/+237650752454
 * tsafack07albin@gmail.com
 * http://gitlab.com/dump501
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class MarkController extends CI_Controller
{
    public $isProfHookable = TRUE;
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->library('encrypt');
        $this->load->model(['Mark', 'Teacher', 'Classroom', 'Subject']);
    }

    public function index()
    {
        $data['classrooms'] = $this->Classroom->getOwn()->result();
        $this->load->view('prof/mark/index', $data);
    }

    public function show()
    {
        /*$this->db->where('id', 0);
        $this->db->from('teachers');
        var_dump($this->db->get());
        die();*/
        //$this->Mark->getWithClassroom($this->input->post('classroom_id'));
        $allInput = $this->input->post('classroom_id');
        $ClassroomSubject = explode("|", $allInput);
        /*var_dump($ClassroomSubject[0]);
        var_dump($ClassroomSubject[1]);
        die();*/
        $student = new Student();
        $student->classroom_id = $ClassroomSubject[0];
        $data['classroom_id'] = $ClassroomSubject[0];
        $data['subject_id'] = $ClassroomSubject[1];
        $data['sequence'] = $this->input->post('sequence');
        $mark = new Mark();
        $data['acad_year'] = $mark->acad_year;
        //$student->byClassroom()->num_rows();
        $data['students'] = $student->byClassroom()->result();
        if(isset($ClassroomSubject[1]))
        {
            $this->load->view('prof/mark/show', $data);
        }
        else
        {
            redirect('dashboard/mark');
        }
    }

    public function create()
    {
        //var_dump();
        die();
        //$this->db->where('student_matricule', $)
        $this->load->view('prof/mark/create');
    }
    

    //api
    //return the marks for a subject on a classroom
    public function apiGetMarks()
    {
        $classroom = $this->input->post('classroom_id');
        $subject = $this->input->post('subject_id');

        $mark = new Mark();
        $mark->classroom_id = $classroom;
        $mark->subject_id = $subject;
        $mark->sequence = $this->input->post('sequence');
        $query = $mark->byClassroomAndSubject();
        $retour['response'] = $query;
        echo json_encode($retour);
    }

    //insert or update the marks of a classroom in a subject
    public function apiUpdateMarks()
    {
        $studentMarks = json_decode($this->input->post('studentMarks'));
        $subject_id = $this->input->post('subject_id');
        $this->db->where('subject_id', $subject_id);
        $this->db->where('student_matricule', $studentMarks[0]->name);
        $this->db->where('sequence', $this->input->post('sequence'));
        $this->db->from('marks');
        $markTest = $this->db->get();
        if($markTest->num_rows() > 0)
        {
            //update
            foreach($studentMarks as $studentMark)
            {
                $mark = new Mark();
                $mark->student_matricule = $studentMark->name;
                $mark->subject_id = $subject_id;
                $mark->sequence = $this->input->post('sequence');
                $mark->mark = $studentMark->mark;
                $mark->update();
            }
            echo json_encode("mark updated successfully");
        }
        else
        {
            //insert
            foreach($studentMarks as $studentMark)
            {
                $mark = new Mark();
                $mark->student_matricule = $studentMark->name;
                $mark->subject_id = $subject_id;
                $mark->sequence = $this->input->post('sequence');
                $mark->mark = $studentMark->mark;
                $mark->save();
            }
            echo json_encode("mark added successfully");
        }
    }

    public function displayForm()
    {
        $data['classrooms'] = $this->Classroom->getOwn()->result();
        $this->load->view('prof/mark/display_form', $data);
    }

    public function displayMarks()
    {
        $allInput = $this->input->post('classroom_id');
        $ClassroomSubject = explode("|", $allInput);
        /*var_dump($ClassroomSubject[0]);
        var_dump($ClassroomSubject[1]);
        die();*/
        $student = new Student();
        $student->classroom_id = $ClassroomSubject[0];
        $data['classroom_id'] = $ClassroomSubject[0];
        $data['subject_id'] = $ClassroomSubject[1];
        $data['sequence'] = $this->input->post('sequence');
        $mark = new Mark();
        $data['acad_year'] = $mark->acad_year;
        //$student->byClassroom()->num_rows();
        $data['students'] = $student->byClassroom()->result();
        if(isset($ClassroomSubject[1]))
        {
            $this->load->view('prof/mark/display_marks', $data);
        }
        else
        {
            redirect('dashboard/mark/display_form');
        }

    }
}