<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class AdminReportBookletController extends CI_Controller
{
    public $isAdminHookable = TRUE;
    public function __construct()
    {
        parent::__construct();
        $this->load->model(['Student', 'Mark', 'Subject']);
    }

    public function index()
    {
        $students = $this->Student->all()->result();
        $this->load->view('admin/report_booklet/index', compact('students'));
    }

    public function print()
    {
        $mark = new Mark();
        $mark->student_matricule = $this->input->post('student_matricule');
        $mark->sequence = $this->input->post('sequence');
        $marks = $mark->byStudentAndSequence();
        //var_dump($mark->byStudentAndSequence());
        //die();
		//$this->load->view('generatepdf', compact('tests'));
		$this->load->library('pdf');
		//$this->load->view('generatepdf');
		$this->pdf->load_view('generate_single_student_report_booklet', compact('marks'));
    }
}