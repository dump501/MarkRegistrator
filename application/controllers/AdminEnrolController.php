<?php
/**
 * by dump501
 * +237698406818/+237650752454
 * tsafack07albin@gmail.com
 * http://gitlab.com/dump501
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminEnrolController extends CI_Controller
{
    public $isAdminHookable = TRUE;
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->library('encrypt');
        $this->load->model(['Enrol', 'Subject', 'Classroom']);
    }

    public function index()
    {
        $query = $this->db->query('SELECT subjects.name as subject_name, classrooms.name as classroom_name, enrols.id as id FROM enrols, subjects,classrooms WHERE subjects.id in (SELECT subject_id FROM enrols) and classrooms.id in (SELECT classroom_id FROM enrols) and subjects.id = enrols.subject_id and classrooms.id = enrols.classroom_id');
        $data['enrols'] = $query->result();
        $this->load->view('admin/enrol/index', $data);
    }

    public function create()
    {
        $data['subjects'] = $this->Subject->all()->result();
        $data['classrooms'] = $this->Classroom->all()->result();
        $this->load->view('admin/enrol/create', $data);
    }

    public function store()
    {
        $this->form_validation->set_rules('subject_id', 'subject', 'required');
        $this->form_validation->set_rules('classroom_id', 'subject', 'required');

        if($this->form_validation->run())
        {
            $enrol = new Enrol();
            $enrol->subject_id = $this->input->post('subject_id');
            $enrol->classroom_id = $this->input->post('classroom_id');
            $enrol->save();
            redirect('admin_dashboard/enrol');
        }
        else
        {
            $this->load->view('admin/enrol/create');
        }
    }

    public function delete($id)
    {
        $enrol = new Enrol();
        $enrol->id = $id;
        $enrol->delete();
        redirect('admin_dashboard/enrol');
    }
}