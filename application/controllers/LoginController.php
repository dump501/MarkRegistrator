<?php
/**
 * by dump501
 * +237698406818/+237650752454
 * tsafack07albin@gmail.com
 * http://gitlab.com/dump501
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class LoginController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->library('encrypt');
        $this->load->model('LoginModel');
    }

    public function login()
    {
        $this->load->view('login');
    }

    public function postLogin()
    {
        $this->form_validation->set_rules('login', 'login', 'required');
        $this->form_validation->set_rules('password', 'password', 'required');


        //$verification_key = md5(rand());
        //$encrypted_password = $this->encrypt->encode("admin");
        //die($encrypted_password);
        if($this->form_validation->run())
        {
            $result = $this->LoginModel->canLogin($this->input->post('login'), $this->input->post('password'));
            if($result == '')
            {
                $this->session->set_flashdata('message', 'Welcome prof !!');
                redirect('dashboard');
            }
            else if($result == 'admin')
            {
                $this->session->set_flashdata('message', 'Welcome Admin !!');
                redirect('admin_dashboard');
            }
            else
            {
                $this->session->set_flashdata('message', $result);
                redirect('login');
            }

        }
        else
        {
            $this->login();
        }
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('home');
    }
}