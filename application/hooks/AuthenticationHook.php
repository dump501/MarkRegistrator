<?php  
defined('BASEPATH') OR exit('No direct script access allowed');  

class AuthenticationHook 
{   private $CI;
    public function __construct()
    {
        $this->CI =& get_instance(); 
    }

    public function isAuthenticated()  
    { 
        if(isset($this->CI->isAdminHookable))
        {
            //die("is hook"); 
            $sessionData = $this->CI->session;
            if(isset($this->CI->session->userdata('auth')[0]["id"]) && $this->CI->session->userdata('auth')[0]["isAdmin"] == 1)
            {
                //continue
            }
            else
            {
                //redirect to login page
                redirect('login');
            }
        }
        if(isset($this->CI->isProfHookable))
        {
            //die("is hook"); 
            $sessionData = $this->CI->session;
            if(isset($this->CI->session->userdata('auth')[0]["id"]) && $this->CI->session->userdata('auth')[0]["isAdmin"] == 0)
            {
                //continue
            }
            else
            {
                //redirect to login page
                redirect('login');
            }
        }
    }  
}  