-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : mar. 26 jan. 2021 à 06:26
-- Version du serveur :  5.7.31
-- Version de PHP : 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `mark_registrator`
--

-- --------------------------------------------------------

--
-- Structure de la table `classrooms`
--

DROP TABLE IF EXISTS `classrooms`;
CREATE TABLE IF NOT EXISTS `classrooms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `acad_year` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `classrooms`
--

INSERT INTO `classrooms` (`id`, `name`, `acad_year`) VALUES
(1, 'form1', '2020/2021'),
(2, 'form2', '2020/2021'),
(3, 'form3', '2020/2021'),
(4, 'form4', '2020/2021'),
(5, 'form5', '2020/2021'),
(6, 'form6', '2020/2021');

-- --------------------------------------------------------

--
-- Structure de la table `enrols`
--

DROP TABLE IF EXISTS `enrols`;
CREATE TABLE IF NOT EXISTS `enrols` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject_id` int(11) NOT NULL,
  `classroom_id` int(11) NOT NULL,
  `acad_year` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `marks`
--

DROP TABLE IF EXISTS `marks`;
CREATE TABLE IF NOT EXISTS `marks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_matricule` varchar(10) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `mark` float NOT NULL,
  `sequence` int(11) NOT NULL,
  `acad_year` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `marks`
--

INSERT INTO `marks` (`id`, `student_matricule`, `subject_id`, `mark`, `sequence`, `acad_year`) VALUES
(1, 'Uba18T0068', 1, 0, 2, '2020/2021'),
(2, ' enim.', 1, 0, 2, '2020/2021'),
(3, 'Uba18T0068', 1, 12, 1, '2020/2021'),
(4, ' enim.', 1, 11, 1, '2020/2021');

-- --------------------------------------------------------

--
-- Structure de la table `school_state`
--

DROP TABLE IF EXISTS `school_state`;
CREATE TABLE IF NOT EXISTS `school_state` (
  `school_name` varchar(200) NOT NULL DEFAULT 'Gouvment height school of test',
  `abr` varchar(10) NOT NULL DEFAULT 'GHST',
  `acad_year` varchar(10) NOT NULL,
  `sequence` int(11) NOT NULL,
  `modification_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `school_state`
--

INSERT INTO `school_state` (`school_name`, `abr`, `acad_year`, `sequence`, `modification_date`) VALUES
('Gouvment height school of test', 'GHST', '2020/2021', 1, '2021-01-17 08:29:53');

-- --------------------------------------------------------

--
-- Structure de la table `students`
--

DROP TABLE IF EXISTS `students`;
CREATE TABLE IF NOT EXISTS `students` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `matricule` varchar(10) NOT NULL,
  `name` varchar(200) NOT NULL,
  `parent_phone` varchar(14) NOT NULL,
  `classroom_id` int(11) NOT NULL,
  `acad_year` varchar(10) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `matricule` (`matricule`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `students`
--

INSERT INTO `students` (`id`, `matricule`, `name`, `parent_phone`, `classroom_id`, `acad_year`) VALUES
(1, 'Uba18T0068', 'Fabiola Orn', '013-542-7368', 1, '2020/2021'),
(2, ' enim.', 'Zella Ondricka', '850-377-3765', 1, '2020/2021'),
(3, 'zert', 'Austen Schneider', '751-448-9383', 2, '2020/2021'),
(4, 'aqs', 'Estevan Smith', '372-676-2072', 2, '2020/2021'),
(5, 'qwxc', 'Lexie Lesch', '393-011-6270', 3, '2020/2021'),
(6, 'sdff', 'Shaun Schimmel', '044-063-7609', 3, '2020/2021');

-- --------------------------------------------------------

--
-- Structure de la table `subjects`
--

DROP TABLE IF EXISTS `subjects`;
CREATE TABLE IF NOT EXISTS `subjects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(15) NOT NULL,
  `name` varchar(200) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `classroom_id` int(11) NOT NULL,
  `acad_year` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `subjects`
--

INSERT INTO `subjects` (`id`, `code`, `name`, `teacher_id`, `classroom_id`, `acad_year`) VALUES
(1, 'math1', 'mathematic1', 4, 1, '2020/2021'),
(2, 'phy1', 'physique 1', 2, 1, '2020/2021'),
(3, 'math3', 'mathematics3', 4, 3, '2020/2021'),
(4, 'phy 2', 'physique2', 6, 2, '2020/2021');

-- --------------------------------------------------------

--
-- Structure de la table `teachers`
--

DROP TABLE IF EXISTS `teachers`;
CREATE TABLE IF NOT EXISTS `teachers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `phone` varchar(13) NOT NULL,
  `login` varchar(200) NOT NULL,
  `password` text NOT NULL,
  `acad_year` varchar(10) NOT NULL,
  `is_admin` char(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `teachers`
--

INSERT INTO `teachers` (`id`, `name`, `phone`, `login`, `password`, `acad_year`, `is_admin`) VALUES
(2, 'Albin Tsafack', '+237698406818', 'admin', 'VDsNNFQ+A29WNQ==', '2020/2021', '1'),
(5, 'Edmund Mayer', '888', 'Consequatur quo dicta harum quas.', 'VQQIbAZlBTYNZwQOUSoHVgBVVQJUcVJnVjIFSVVG', '2020/2021', '0'),
(4, 'Albertha Ferry', '123', 'teacher', 'UihcZAJkBmAGY1JrA3M=', '2020/2021', '0'),
(6, 'Alexandrine Batz', '734-359-3933', 'A debitis in consequatur.', 'UThdUgl4AGNUDFVvDDgBbQJRUhxRSlhuVmJVLlRL', '2020/2021', '0'),
(7, 'Mozell Yundt', '765-658-8585', 'Fuga quidem perferendis aperiam eum possimus qui delectus qui.', 'VmhaUFQkBDdXOV1ZB1MBQQJLBk1begMgVAhQJwIM', '2020/2021', '0'),
(8, 'Luis Beier', '734-440-9592', 'Recusandae aut quia quod dolores.', 'BEUOEABQB3sAegYVVj1WB1cFA3VUagItA3NXMAJh', '2020/2021', '0');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
